import React, { Component } from 'react';
import { Card, CardImg, CardText, CardBody, CardTitle } from 'reactstrap';

class DishDetail extends Component {

    constructor(props) {
        super(props);
    }

    renderDish(dish) {
            return (
                <Card>
                    <CardImg width="100%" src={dish.image} alt={dish.name} />
                    <CardBody>
                        <CardTitle> {dish.name}</CardTitle>
                        <CardText> {dish.description} </CardText>
                    </CardBody>
                </Card>
            );
    }

    renderComments(comments) {
        const commentContent = comments.map((comment) => {
            return (
                <div key={comment.id}>
                    <div className="container">
                        <ul className="list-unstyled">
                            <li> {comment.comment} </li>
                        </ul>
                        <ul className="list-unstyled">
                            <li> --{comment.author}, {comment.date} </li>
                        </ul>
                    </div>
                </div>
                );
        });
                //const menu = this.props.dishes.map((dish) => {

        return (
            <div className="container">
                <h1> Comments </h1>
                <div> {commentContent} </div>
            </div>
        );
    }

    render() {
        if (this.props.dish != null) {
            return (
                    <div className="row">
                        <div className="col-12 col-md-5 md-1">
                            {this.renderDish(this.props.dish)}
                        </div>
                        <div className="col-12 col-md-5 md-1">
                            {this.renderComments(this.props.dish.comments)}
                        </div>
                    </div>
                );
        }
        else
        {
            return (
                <div></div>
            );
        }
    }

}

export default DishDetail;